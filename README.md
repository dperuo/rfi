# Renaissance Fitness Inc.

## Fitness supplements, simplified.
Choosing fitness supplements can be hard. Which are good? Which are bad? Which actually work? Which are just a waist of time?

Imagine if there was a better way to pick fitness supplements. 

That's why we're here.

Renaissance Fitness was created to simplify your life. 

We sell fitness supplements based on fitness goals—Weight Loss, Muscle Gain, Stress Relief, that sort of thing—and each kits includes a full month’s supply of all the supplements needed to help reach that goal.

These kits are NOT sample packs. They contain everything you need for the month so there’s no need to go back to buy more until next month.

Plus these kits contain only high-quality products. Products we’ve used ourselves, that actually work, and are proven safe.

Basically, we’ve done all the research so you don’t have to!

Please [browse our store](http://renaissance-fitness-inc.myshopify.com/collections/all), [read our reviews](http://renaissance-fitness-inc.myshopify.com/blogs/news), or [check out our FAQ page](http://renaissance-fitness-inc.myshopify.com/pages/faq).

## Success Stories
> If you're looking to get healthy or stay healthy, work with Renaissance Fitness! I have achilles tendinitis and was sucking up pills all day to cope with the pain. The team at Renaissance Fitness recommended I start taking fish oil and <strong>now the pain is completely gone!</strong> I'm glad it works, because I didn't know what else to do. The fish oil really does get rid of inflammation, just like they said. Anyone with arthritis or inflammation would benefit from what these guys have to offer!&nbsp;~&nbsp;Laurie A.

> I would be miserable if I had to stop training with Renaissance Fitness! They really understand my lifestyle, and are great for busy people like myself. They makes it easy to schedule sessions, and <strong>the results have been awesome</strong>. I feel great and haven&rsquo;t been in this good of shape since high school! &nbsp;~&nbsp;Scott L.

> In a world full of trainers who are complacent with their education and ability, <strong>the team at Renaissance Fitness is consistently pushing their clients&rsquo; and their own boundaries and separating themselves from the pack.</strong> &nbsp;~&nbsp;Matt McGorry, mattmcgtraining.com 

> Since high school, I have made repeated attempts to stick to a workout routine of my own, but I always end up slacking somehow. Working with Renaissance Fitness helped me break that pattern. They keep me motivated and reminded of my goals &mdash; real strength, large range of mobility, and a healthy, fit, muscular body. I particularly appreciate how well they listen to my concerns. Most of all, I value their dedication to crafting a constantly-changing program to suit my specific needs and abilities. <strong>Work with these guys!</strong> &nbsp;~&nbsp;Colin S. 

> Renaissance Fitness was absolutely essential in me being able to stick to that all too common New Years Resolution. I went from couch potato to gym regular and have been faithful for over a year. I was intimidated by the weight room but the guys at Renaissance taught me the basics of how to effectively train. <strong>They quickly identified my weaknesses, personalized a routine and made me stronger in ways I didn&rsquo;t expect.</strong> They helped me more than just be happy with my body: I now love the gym and am able to identify my own weaknesses! If you want trainers dedicated to their clients, these are your guys!&nbsp;~&nbsp;Patrick R.

> I get so much out of my workouts because they are right there motivating me and teaching me proper technique. <strong>I always go 100% because they inspire me to put forth my best.</strong> I could not be happier! &nbsp;~&nbsp;Charlie D. 

> Aside from being a very knowledgeable and efficient trainers, they were also incredibly personable and friendly! I looked forward to every session we had together because they made me feel motivated and comfortable. <strong>Our work together has helped me to pass my college soccer preseason fitness training, and there is no way that I would have been able to achieve such a goal without them</strong>. Thank you so much again! &nbsp;~&nbsp;Harrison F. 

## Published In
![Published in Men's Fitness, Men's Health, and Muscle & Fitness](http://cdn.shopify.com/s/files/1/0204/2560/files/RFI_Press.png)