---
layout: post
title: Relax Better with Yogi Bedtime Tea
author: Derek Peruo
contact: mailto:derek@renaissancefitnessinc.com
---
![Tea Time!][]

## [Star Rating:][] &#9733;&#9733;&#9733;&#9734;&#9734;

Let's face it: we all work hard, train hard, put in long hours during the day, and are basically stressed to the max.  This means rest and relaxation are vital for us to stay at the top of our game, both at work and at the gym.

My personal relaxation routine includes a “pre-bed tea,” something I can drink shortly before going to sleep that helps calm my nerves.

My favorite teas are herbal teas&mdash;preferably something that includes chamomile, lavender and licorice.  This not only helps with relaxation, but also promotes good digestion!

Lucky for me, tea has a [long and noble history][wiki], full of endless variations.  One such variation is Yogi Bedtime Tea, which my friends at [eVitamins.com][] were happy to let me review.

I like relaxation teas that smell good, that are easy to prepare, and that make me feel satisfied after one or two cups.  Yogi Bedtime Tea meets all of these requirements! The tea includes licorice root, chamomile, raspberry, rose hip, and lavender, as well as spearmint for a mild cooling sensation.

**A WORD OF CAUTION:** Yogi Bedtime Tea contains stevia leaf, which gives me migraines.  You may have a similar reaction, so if you're sensitive to stevia like I am, check out some great alternatives like Trader Joe's Bedtime Tea, and Sleepytime Herb Tea from Celestial Seasonings.

![More Tea Options][]

If you can consume stevia without issue, I recommend giving Yogi Bedtime Tea a try.  It's sweet taste and easy preparation make it an ideal addition to any relaxation routine.

Learn more at [eVitamins.com][product].

[More Tea Options]: {{ site.url }}/assets/tea-time-options.jpg
[Tea Time!]: {{ site.url }}/assets/tea-time.jpg
[wiki]: http://en.wikipedia.org/wiki/Tea
[eVitamins.com]: http://evitamins.com
[product]: http://www.evitamins.com/bedtime-organic-tea-yogi-tea-organic-tea-7980
[Star Rating:]: {{ site.url }}/how-we-rate-products.html